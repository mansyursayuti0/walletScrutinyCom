---
title: "Lite HD Wallet – Your Coin Base"
altTitle: 

users: 500
appId: org.freewallet.lite.android
launchDate: 
latestUpdate: 2019-03-01
apkVersionName: "Varies with device"
stars: 
ratings: 
reviews: 
size: Varies with device
website: https://freewallet.org
repository: 
issue: 
icon: org.freewallet.lite.android.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-11-29
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /lite/
  - /org.freewallet.lite.android/
  - /posts/2019/11/lite/
  - /posts/org.freewallet.lite.android/
---


**Update**: This wallet shares no source code but was categorized as too small,
which makes it irrelevant for some listings.

In their description they claim

> Keys are always kept in your phone and never sent outside your device.

so it's a non-custodial wallet.

Neither on Google Play nor on their website could we find a link to the source
code.

[Searching their application ID on GitHub](https://github.com/search?q="org.freewallet.lite.android")
was without results, too.

This wallet is **not verifiable**.
