---
title: "ChangeNOW – Limitless Crypto Exchange"
altTitle: 

users: 10000
appId: io.changenow.changenow
launchDate: 
latestUpdate: 2020-10-25
apkVersionName: "1.99"
stars: 4.7
ratings: 411
reviews: 236
size: 5.7M
website: http://changenow.io
repository: 
issue: 
icon: io.changenow.changenow.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ChangeNOW_io
providerLinkedIn: 
providerFacebook: ChangeNOW.io
providerReddit: ChangeNOW_io

redirect_from:
  - /io.changenow.changenow/
---


> We focus on simplicity and safety — the service is registration-free and non-custodial.

> With ChangeNOW, you remain in full control over your digital assets.

That's a claim. Let's see if it is verifiable ...

There is no claim of public source anywhere and
[neither does GitHub know](https://github.com/search?q=%22io.changenow.changenow%22)
this app, so it's at best closed source and thus **not verifiable**.
