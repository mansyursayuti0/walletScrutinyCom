---
title: "Swap Wallet - BTC, Bitcoin wallet"
altTitle: 

users: 50000
appId: com.swapwalletltd.swap
launchDate: 
latestUpdate: 2020-11-14
apkVersionName: "Varies with device"
stars: 4.9
ratings: 2077
reviews: 1861
size: Varies with device
website: http://swapwallet.com
repository: 
issue: 
icon: com.swapwalletltd.swap.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.swapwalletltd.swap/
---


We can find no information on security. Also their links to Knowledge Base and
FAQ are broken. Like this we have to assume it's a custodial wallet and thus
**not verifiable**.