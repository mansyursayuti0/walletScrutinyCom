---
title: "Altcoin Bitcoin Trade"
altTitle: 

users: 5000
appId: com.mk.cryptofolio
launchDate: 
latestUpdate: 2020-08-19
apkVersionName: "1.0.51"
stars: 4.7
ratings: 17
reviews: 10
size: 8.0M
website: http://www.thecryptofolioapp.com
repository: 
issue: 
icon: com.mk.cryptofolio.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-06-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.mk.cryptofolio/
  - /posts/com.mk.cryptofolio/
---


This app is an interface for multiple exchanges:

> The CryptoFolio App™ lets you trade instantly on suported exchanges without
  signing in to them every time.

meaning you don't own your keys (The keys to the exchanges maybe but not to your
bitcoins). The app security is therefore **not verifiable**.
