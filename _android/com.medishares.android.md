---
title: "MathWallet: Bitcoin,Ethereum,EOS,Polkadot,Cosmos"
altTitle: 

users: 10000
appId: com.medishares.android
launchDate: 
latestUpdate: 2020-12-03
apkVersionName: "3.8.7"
stars: 4.0
ratings: 656
reviews: 345
size: 51M
website: https://mathwallet.org
repository: 
issue: 
icon: com.medishares.android.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-03
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Mathwallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.medishares.android/
---


This wallet claims:

> - Secure your assets with Private Keys, Mnemonic Phrases, 2-Factor Authentication, and more

which kind of sort of sounds like a non-custodial wallet but also doesn't make
much sense. The private keys are what you want to protect. They are not a tool
to protect something.

[Their website](http://www.medishares.org/)
as per the dedicated data field on Google Play appears to not link
back to the wallet but in the description they mention a different website:
[mathwallet.org](https://mathwallet.org).

There we find no further claims about who holds the keys or public source code.

As they also promote a [Math Cloud Wallet](https://mathwallet.org/mathcloud/en/)
which is

> Convenient, safe and easy to use custodial wallet

we assume the wallet here is meant to be non-custodial but as it's closed source,
it is **not verifiable**.
