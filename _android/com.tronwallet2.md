---
title: "TronWallet: Bitcoin Blockchain Wallet"
altTitle: 

users: 1000000
appId: com.tronwallet2
launchDate: 
latestUpdate: 2020-07-23
apkVersionName: "3.4.5"
stars: 3.9
ratings: 8355
reviews: 4574
size: Varies with device
website: https://www.tronwallet.me
repository: 
issue: 
icon: com.tronwallet2.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-17
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.tronwallet2/
---


> TronWallet uses advanced security mechanisms that completely protects the
  user’s private keys, and makes private keys and sensitive data available only
  on the user’s specific device, utilizing the latest military-grade technology
  for encryption.

and

> No third party, including TronWallet, can access your private keys or restrict
  any transaction the user decides to make.

are clear statements of this app being a non-custodial wallet.

> \* No fees - Yes, that’s right - send and receive is free

This is a suspicious claim as Bitcoin transactions cannot be sent for free any
more.

> \* Your keys are yours and PRIVATE to you alone - YOU truly own your crypto

and

> IMPORTANT NOTE REGARDING SECURITY AND PRIVACY:<br>
  \* Your Private Key is stored encrypted on your smartphone. It is never shared
  or sent anywhere. Nobody at the TronWallet organization can access your
  private key.

They really care to stress this point :) Let's see if we can verify this claim.

So [their website as registered in Google Play](https://www.tronwallet.me/)
forwards us to [this website](https://klever.io/en/) which has a link to
download another Bitcoin wallet:
[Klever: Bitcoin Blockchain Wallet](/cash.klever.blockchain.wallet/).

As the website is not about the app, the Google Play description does not
point to any source code and neither can we
[find a relevant project on GitHub](https://github.com/search?q=%22com.tronwallet2%22&type=code),
we assume this app is closed source and thus **not verifiable**.
