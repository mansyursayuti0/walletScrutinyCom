---
title: "Zumo - Bitcoin Wallet App - Buy, Store, & Transfer"
altTitle: 

users: 1000
appId: com.zumopay.core
launchDate: 
latestUpdate: 2020-11-30
apkVersionName: "2.12.1"
stars: 3.9
ratings: 62
reviews: 49
size: 63M
website: https://zumo.money
repository: 
issue: 
icon: com.zumopay.core.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-10-12
reviewStale: true
signer: 
reviewArchive:


providerTwitter: zumopay
providerLinkedIn: company/zumomoney
providerFacebook: zumo.money
providerReddit: 

redirect_from:
  - /com.zumopay.core/
  - /posts/com.zumopay.core/
---


> 💰 Provides full ownership of funds - your crypto is only owned by you!

sounds like non-custodial but

> 👮 Accounts are activated after an ID check!

doesn't. Then again

> 📵 Lost your device? Reinstall, Login, & use your Backup Phrase to regain
  access

says something about "Backup Phrase" but if you have to "Login" first, we wonder
if that backup phrase is shared with a server or if it is a BIP39 mnemonic at
all.

> Your Private Keys are managed on device, crypto currency is stored securely on
  the blockchain, and all of our codebase is developed in-house.

Again, this does not say anything about the keys being **exclusively** being on
the device. Also what is that claim about the codebase? Well, let's see ...

> As a non-custodial wallet, all your crypto is stored on the Blockchain.

Now that is explicit. Let's see if there is public source code available ...

... but we can't find anything on Google Play or their website. The verdict is
thus: **not verifiable**.
