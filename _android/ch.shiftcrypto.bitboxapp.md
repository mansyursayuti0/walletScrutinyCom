---
title: "BitBoxApp"
altTitle: 

users: 500
appId: ch.shiftcrypto.bitboxapp
launchDate: 
latestUpdate: 2020-12-12
apkVersionName: "android-4.24.1"
stars: 4.5
ratings: 12
reviews: 6
size: 63M
website: 
repository: 
issue: 
icon: ch.shiftcrypto.bitboxapp.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-08-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /ch.shiftcrypto.bitboxapp/
---


