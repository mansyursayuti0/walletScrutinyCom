---
title: "BitWallet - Buy & Sell Bitcoin"
altTitle: 

users: 10000
appId: com.Android.Inc.bitwallet
launchDate: 
latestUpdate: 2020-12-02
apkVersionName: "1.4.10"
stars: 4.3
ratings: 410
reviews: 297
size: 26M
website: https://www.bitwallet.org
repository: 
issue: 
icon: com.Android.Inc.bitwallet.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: bitwalletinc
providerLinkedIn: 
providerFacebook: BitWalletInc
providerReddit: 

redirect_from:
  - /com.Android.Inc.bitwallet/
  - /posts/com.Android.Inc.bitwallet/
---


This appears to be primarily an exchange and as there are no claims of you being
in sole control of your funds, we have to assume it is a custodial service and
therefore **not verifiable**.
