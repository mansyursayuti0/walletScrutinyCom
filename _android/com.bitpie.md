---
title: "Bitpie Wallet - Bitcoin USDT ETH EOS BCH TRON LTC"
altTitle: 

users: 50000
appId: com.bitpie
launchDate: 2016-10-23
latestUpdate: 2020-12-09
apkVersionName: "5.0.005"
stars: 3.8
ratings: 661
reviews: 338
size: 57M
website: http://bitpie.com
repository: 
issue: 
icon: com.bitpie.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-25
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BitpieWallet
providerLinkedIn: 
providerFacebook: BitpieOfficial
providerReddit: BitpieWallet

redirect_from:
  - /com.bitpie/
  - /posts/com.bitpie/
---


This app has bold claims:

> Bitpie Wallet is the world's leading multi-chain wallet

Many apps we review, claim to have a unique position somehow. For now we just
assume it claims to be a wallet.

> Access to BitHD Cold wallet;

What exactly does that mean? **access** to **cold wallet** sounds like an
oxymoron assuming a cold wallet is
defined as one that is offline. (**Update**: As we later found out, they also
provide a hardware wallet. In how far a bluetooth connected hardware wallet can
be considered a "cold" wallet is debatable though, too. But they share that
hardware wallet's [source code](https://github.com/bithd).)

Here there are the key words we want to read:

> As a true decentralized wallet, your private key will never leave the device.

It is non-custodial. At least so they claim. Let's see if it is reproducible ...

In the description we find no link to their source code. On the website ...

(The provider might want to change those download links "Google down" could be
mistaken as the link being "down" as in "unavailable".)

Unfortunately we cannot find any code for the Android wallet and thus have to
give the verdict: **not verifiable**.
