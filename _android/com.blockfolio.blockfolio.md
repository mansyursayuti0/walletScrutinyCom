---
title: "Blockfolio - Bitcoin and Cryptocurrency Tracker"
altTitle: 

users: 1000000
appId: com.blockfolio.blockfolio
launchDate: 2015-10-01
latestUpdate: 2020-06-11
apkVersionName: "2.5.10"
stars: 4.7
ratings: 109461
reviews: 33350
size: 49M
website: https://www.blockfolio.com
repository: 
issue: 
icon: com.blockfolio.blockfolio.png
bugbounty: 
verdict: nowallet # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-11-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /blockfolio/
  - /com.blockfolio.blockfolio/
  - /posts/2019/11/blockfolio/
  - /posts/com.blockfolio.blockfolio/
---


This is not a wallet.
