---
title: "Mycelium Bitcoin Wallet"
altTitle: 

users: 1000000
appId: com.mycelium.wallet
launchDate: 2013-07-01
latestUpdate: 2020-11-26
apkVersionName: "3.7.0.5"
stars: 4.1
ratings: 9274
reviews: 4263
size: 23M
website: https://wallet.mycelium.com
repository: https://github.com/mycelium-com/wallet-android
issue: 
icon: com.mycelium.wallet.png
bugbounty: 
verdict: reproducible # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-17
reviewStale: true
signer: b8e59d4a60b65290efb2716319e50b94e298d7a72c76c2119eb7d8d3afac302e
reviewArchive:
- date: 2020-07-13
  version: "3.5.3.0"
  apkHash: 847f61d6d6f24a459cba720adf0d9e1fd052431a2b49565ca424c13860a29f59
  gitRevision: 2caf243a5a30d549f351017369501795825d9759
  verdict: reproducible
- date: 2020-07-13
  version: "3.5.2.0"
  apkHash: 4e5cd2d3d13a06e11a0d4ecf4c4528bfea8f232dc60a1a344124cedae86b8430
  gitRevision: 7d140e2e8f72e0fbe5a2bc325219432c4f9791e6
  verdict: reproducible
- date: 2020-07-13
  version: "3.5.1.0"
  apkHash: bd968366d76267434fa067d4ef44f1fd3efab6144858fe752873be697633502f
  gitRevision: a1d83d77193224c49e56828022767399d2de8968
  verdict: reproducible
- date: 2020-06-17
  version: "3.5.0.1"
  apkHash: 7fcf353ca112f61ebc16f4a5ab1243ffeb012a87cca8d143fb708bdf5f8de559
  gitRevision: dcfda3c88223331de00a6d87dcc43dc2be8240e9
  verdict: reproducible
- date: 2020-06-17
  version: "3.5.0.0"
  apkHash: e8279deb438df1dfac458a2808073aafd0d10f54b11bb4e867561e71dc852bcc
  gitRevision: 6f2c91e407487ce1b31a6096f1a57fd734c88f38
  verdict: reproducible
- date: 2020-06-17
  version: "3.4.0.3"
  apkHash: 275053f9910e1402eb63f105c5791d6cc1b3f2c28f281602da629b256ab115e6
  gitRevision: 79aea7a92b35c1344c4262b5496f2483425dc06b
  verdict: reproducible
- date: 2020-05-06
  version: "3.4.0.1"
  apkHash: 7c6bbc62fdd429b60ac0d1877201fb1f34287c176a84f7d130f466d3c4947777
  gitRevision: b5f335a161074d58b6b94aec9c3a0056de79cd50
  verdict: reproducible
- date: 2020-05-06
  version: "3.4.0.0"
  apkHash: 35d7eeafa87ce88d527c9a41865eaa4cdcd158be8ea190c84133fbb02bfb6c46
  gitRevision: ca8600b612168fbac4e5a6a297664e72c82fe0c6
  verdict: reproducible
- date: 2020-04-27
  version: "3.4.0.0"
  apkHash: 35d7eeafa87ce88d527c9a41865eaa4cdcd158be8ea190c84133fbb02bfb6c46
  gitRevision: 73e728868398ecc3c370d0d12ff00f24c6c97e30
  verdict: nonverifiable
- date: 2020-03-20
  version: "3.3.3.1"
  apkHash: f35760dbc40959142c98abf923e70681e75cd6644892be15a7c3d3a689e11af8
  gitRevision: a6b2771dbc314160ba304573fd0a6cc5d6d1ccb9
  verdict: reproducible
- date: 2020-03-20
  version: "3.3.2.1"
  apkHash: b47ea1f72443281aaf6405c52fac0c7747b064dae8f282307a5dfae737e6328b
  gitRevision: 53d2766a0d74bd19375c451e81584c85381a4435
  verdict: reproducible
- date: 2020-02-17
  version: "3.3.1.1"
  apkHash: 6549d022684cac8521ff4c97cdd205ef289c58b58a589e092f1a5439aaf06a59
  gitRevision: b15e44f34278affacdaefaa6bc77d65cb75fba95
  verdict: reproducible
- date: 2020-02-16
  version: "3.3.1.1"
  apkHash: 6549d022684cac8521ff4c97cdd205ef289c58b58a589e092f1a5439aaf06a59
  gitRevision: a1e65039b8ae5efa874a3179e979451fc8b83e44
  verdict: nonverifiable
- date: 2020-01-07
  version: "3.2.0.17"
  apkHash: d0e943c9a974ddcfeb96baa06483b92f22e24e8ed7acada169a17679fcf28ac4
  gitRevision: 26054c022045897e0458d6abe7983e8430c7ae80
  verdict: reproducible
- date: 2019-11-16
  version: "3.0.0.23"
  apkHash: 27e236a723598d058ee3e1c72d54c84983489c8e80f7edee3df52235a0231c8c
  gitRevision: 3d972d9773b0fd2fb1602d31117a50be01d48610
  verdict: reproducible

providerTwitter: MyceliumCom
providerLinkedIn: company/mycelium
providerFacebook: myceliumcom
providerReddit: mycelium

redirect_from:
  - /mycelium/
  - /com.mycelium.wallet/
  - /posts/2019/11/mycelium/
  - /posts/com.mycelium.wallet/
---


**Disclaimer**: The authors of this project have contributed to Mycelium.
Andreas Schildbach was so kind and independently tested and
confirmed Mycelium's verifiability
[here](https://github.com/bitcoin-dot-org/bitcoin.org/issues/3221#issuecomment-566466894)
for a past version. Independent tests would be highly welcome!

Here we test if the latest version also can be verified, following the known
procedure expressed in our
[test script](https://gitlab.com/walletscrutiny/walletScrutinyCom/blob/master/test.sh):

As the [reproducibility issue](https://github.com/docker/for-linux/issues/1018)
was not resolved yet, so the following results were obtained on a
[DigitalOcean](https://m.do.co/c/a0f4504a8b58) server:

```
Results:
appId:          com.mycelium.wallet
signer:         b8e59d4a60b65290efb2716319e50b94e298d7a72c76c2119eb7d8d3afac302e
apkVersionName: 3.7.0.1
apkVersionCode: 3070001
apkHash:        f504ec63d60584a7d850dfe79ce586fd97a72a5c844967d83c642fb9e1bf0c0c

Diff:
Files /tmp/fromPlay_com.mycelium.wallet_3070001/apktool.yml and /tmp/fromBuild_com.mycelium.wallet_3070001/apktool.yml differ
Files /tmp/fromPlay_com.mycelium.wallet_3070001/original/META-INF/CERT.RSA and /tmp/fromBuild_com.mycelium.wallet_3070001/original/META-INF/CERT.RSA differ

Revision, tag (and its signature):
```

This diff is what we want to see to call it **reproducible**.