---
title: "ezDeFi - Crypto & Bitcoin Wallet"
altTitle: 

users: 1000
appId: com.ezdefi
launchDate: 
latestUpdate: 2020-11-22
apkVersionName: "0.2.6"
stars: 4.8
ratings: 24
reviews: 18
size: 15M
website: https://ezdefi.com/
repository: 
issue: 
icon: com.ezdefi.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: false
signer: 
reviewArchive:


providerTwitter: ezDeFi
providerLinkedIn: 
providerFacebook: ezdefi
providerReddit: 

redirect_from:
  - /com.ezdefi/
---


Features like

> By eliminating encryption phrase, new users can simply make purchases with
  just a wallet password or biometric.

sound very custodial. Althogh this is

> A new Ez Mode [...] to make cryptocurrencies accessible to new users.

there are no explicit claims about the app being non-custodial otherwise, which
is why we have to assume it's custodial all the way and thus **not verifiable**.