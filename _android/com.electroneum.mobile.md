---
title: "Electroneum"
altTitle: 

users: 1000000
appId: com.electroneum.mobile
launchDate: 
latestUpdate: 2020-06-19
apkVersionName: "4.6.5"
stars: 3.1
ratings: 54788
reviews: 35784
size: 15M
website: 
repository: 
issue: 
icon: com.electroneum.mobile.png
bugbounty: 
verdict: nobtc # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-06-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.electroneum.mobile/
  - /posts/com.electroneum.mobile/
---


This app does not support storing BTC.

*(Besides that, we couldn't find any source code or even a claim of it being
non-custodial.)*
