---
title: "Binance.US: Buy Bitcoin with USD.  Crypto Wallet"
altTitle: 

users: 100000
appId: com.binance.us
launchDate: 
latestUpdate: 2020-11-14
apkVersionName: "1.3.0"
stars: 3.0
ratings: 710
reviews: 476
size: Varies with device
website: https://www.binance.us
repository: 
issue: 
icon: com.binance.us.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-17
reviewStale: false
signer: 
reviewArchive:


providerTwitter: binanceus
providerLinkedIn: company/binance-us
providerFacebook: BinanceUS
providerReddit: 

redirect_from:
  - /com.binance.us/
---


Binance being a big exchange, the description on Google Play only mentions
security features like FDIC insurance for USD balance but no word on
self-custody. Their website is not providing more information neither. We
assume the app is a custodial offering and therefore **not verifiable**.