---
title: "Best Wallet :: Crypto Currency Wallet"

users: 0
appId: com.cryptocurrencys.bestwallet
ratings: 0
icon: com.cryptocurrencys.bestwallet.png
verdict: defunct
date: 2020-12-04
reviewStale: false
---

This app still appears in search results but Google Play doesn't show the app.
At least not for users in 
[Chile](https://play.google.com/store/apps/details?id=com.cryptocurrencys.bestwallet&hl=es&gl=CL),
[Germany](https://play.google.com/store/apps/details?id=com.cryptocurrencys.bestwallet&hl=de&gl=DE)
or the
[US](https://play.google.com/store/apps/details?id=com.cryptocurrencys.bestwallet&hl=en&gl=US).

In [this video](https://www.youtube.com/watch?v=eFfECTyRuos) you see in the
beginning of the actual demo "Welcome to [Coinomi](/coinomi/) Wallet" which it
was an unauthorized clone of.

Its provider [NICE APPER](https://apk.support/developer/NICE+APPER) also has/had
a wallet called "Trast Wallets" which looks quite similar to
[Trust Crypto Wallet](/trust/) and to little surprise also has a similar icon.

We would have loved to have discovered this app earlier but it looks like a good
idea to not install it, should it somehow return.