---
title: "Monarch Wallet"
altTitle: 

users: 50000
appId: com.sf.monarch
launchDate: 2018-10-19
latestUpdate: 2020-09-18
apkVersionName: "2.1.6"
stars: 3.5
ratings: 724
reviews: 374
size: 18M
website: https://monarchwallet.com
repository: 
issue: 
icon: com.sf.monarch.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-04-12
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Monarchtoken
providerLinkedIn: company/monarchtoken
providerFacebook: MonarchWallet
providerReddit: MonarchToken

redirect_from:
  - /com.sf.monarch/
  - /posts/com.sf.monarch/
---


This app appears to have been created for Monarch Tokens but it also features a
Bitcoin wallet.

Their website according to Google Play is [splashfactory.com](http://splashfactory.com/) but there is
no information there and we found [monarchwallet.com](https://monarchwallet.com) to likely be their
website.

There, we found
[this question in their FAQ](https://monarch.freshdesk.com/support/solutions/articles/44001516779-lost-my-seed):

> **Lost My Seed** Wow this is a problem. We're so sorry, we are a decentralized
> wallet, this means you and only you own your seed. If you lost your seed there
> is nothing we can do for you.

which means they claim to be a non-custodial wallet. But can we find the source
code?

We see no link to source code on Google Play or on their website(s) and also a
[search on GitHub](https://github.com/search?q=%22com.sf.monarch%22&type=Code)
yields no results which leaves us to assume this app is closed source and
therefore **not verifiable**.