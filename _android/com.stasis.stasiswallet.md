---
title: "STASIS Stablecoin Wallet"
altTitle: 

users: 5000
appId: com.stasis.stasiswallet
launchDate: 2018-06-13
latestUpdate: 2020-12-15
apkVersionName: "1.8.85"
stars: 4.5
ratings: 148
reviews: 89
size: 24M
website: https://stasis.net/wallet
repository: https://github.com/stasisnet
issue: 
icon: com.stasis.stasiswallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-03
reviewStale: true
signer: 
reviewArchive:


providerTwitter: stasisnet
providerLinkedIn: company/stasisnet/
providerFacebook: stasisnet
providerReddit: 

redirect_from:
  - /com.stasis.stasiswallet/
  - /posts/com.stasis.stasiswallet/
---


On Google Play and their website there is no mention of being non-custodial and
certainly there is no source code available. Until we hear opposing claims
we consider it a custodial app and therefore **not verifiable**.