---
title: "Swipe Wallet"
altTitle: 

users: 50000
appId: com.swipe.wallet
launchDate: 
latestUpdate: 2020-12-03
apkVersionName: "1.519"
stars: 2.6
ratings: 1125
reviews: 829
size: 53M
website: https://swipe.io
repository: 
issue: 
icon: com.swipe.wallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: SwipeWallet
providerLinkedIn: 
providerFacebook: Swipe
providerReddit: 

redirect_from:
  - /com.swipe.wallet/
---


This app is a custodial offering:

> SECURE STORAGE
> 
> Swipe users can have peace-of-mind knowing their assets are covered under a $100M insurance policy with our custodian. All User deposited funds are stored in cold storage with a trusted custodian. Having these funds in a cold storage wallet ensures our users that their funds are safe and easily accessible through the Swipe Network on the Swipe Wallet

This contradicts itself. Being in cold storage should mean that it's precisely
not easily accessible via network. What's on the network is by definition a
"hot" wallet.

The website lists "coinbase | custody" and "BitGo" as custodians, which means
you not only have to trust them to but also two other services to make sure your
and all the other clients' funds are being accounted for with funds in their
custody.

Anyway, this is all **not verifiable**.
