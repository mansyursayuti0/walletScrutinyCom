---
title: "Mercuryo Bitcoin Cryptowallet"
altTitle: 

users: 100000
appId: com.mercuryo.app
launchDate: 
latestUpdate: 2020-12-16
apkVersionName: "1.10.8"
stars: 4.5
ratings: 1639
reviews: 1008
size: 41M
website: https://mercuryo.io
repository: 
issue: 
icon: com.mercuryo.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-17
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Mercuryo_io
providerLinkedIn: company/mercuryo-io
providerFacebook: mercuryo.io
providerReddit: mercuryo

redirect_from:
  - /com.mercuryo.app/
---


This app has a strong focus on cashing in and out with linked cards and low
exchange fees but no word on who holds the keys. At least not on Google Play.
On their website we find:

> Your private key is safely stored and fully restorable thanks to customer
  verification. Cryptocurrency is stored in safe offline wallets.

which is the definition of a custodial app. This wallet is **not verifiable**.