---
title: "OKEX - خرید بیت کوین"
altTitle: 

users: 10000
appId: co.okex.app
launchDate: 
latestUpdate: 2020-12-06
apkVersionName: "2.6.0"
stars: 3.8
ratings: 544
reviews: 239
size: 12M
website: https://ok-ex.co
repository: 
issue: 
icon: co.okex.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /co.okex.app/
---


> The okex app is a digital currency trading platform

as such, this is probably a custodial offering.

As the website is broken, we can't find any contrary claims and conclude, this
app is **not verifiable**.