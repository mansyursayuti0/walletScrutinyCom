---
title: "Wasabi Wallet"
altTitle: 

users: 500
appId: com.wasabiwallet.dev
launchDate: 
latestUpdate: 2020-12-01
apkVersionName: "2.0"
stars: 3.8
ratings: 25
reviews: 14
size: 22M
website: 
repository: 
issue: 
icon: com.wasabiwallet.dev.png
bugbounty: 
verdict: defunct # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-08
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.wasabiwallet.dev/
---


**Update:** This wallet was removed from Google Play. Discussions of it happened
a bit on Twitter but it was an obvious scam.

**Do not use this app! We expect it to be removed from Google Play. More details
soon.**

This page was created by a script from the **appId** "com.wasabiwallet.dev" and public
information found
[here](https://play.google.com/store/apps/details?id=com.wasabiwallet.dev).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.