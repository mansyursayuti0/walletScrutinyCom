---
title: "Bitcoin India Wallet & Exchange"
altTitle: 

users: 50000
appId: com.bitcoinindia.Btciapp
launchDate: 
latestUpdate: 2020-09-05
apkVersionName: "3.1.24"
stars: 2.5
ratings: 989
reviews: 719
size: 18M
website: https://www.bitcoin-india.org
repository: 
issue: 
icon: com.bitcoinindia.Btciapp.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.bitcoinindia.Btciapp/
---


> India’s Most Secure Bitcoin Wallet

Ok, sure. :)

> • You are the only one who can control and have access to your wallet.

That's a claim of being non-custodial?

> - If your e-mail gets compromised, only you can access your Bitcoin India wallet, because you would still have 2fa on your phone at Google Authenticator.

If the email is relevant for your security, somebody can grant you access and
that somebody ... has access. That does not sound non-custodial.

> - All transactions are synced across all of your devices in real time and are protected by an intense bank-level security protocol.

Now if we would give them the benefit of the doubt and assume you restored the
same seed on all devices then yes, the blockchain would make sure of keeping
your devices in sync but somehow "protected by an intense bank-level security
protocol" does induce the opposite of trust.

Oh, the rating is 2.4 stars. Apparently this is a bad product that doesn't
bother to buy 5 star ratings like other scammy apps.

Many reviews read like [this](https://play.google.com/store/apps/details?id=com.bitcoinindia.Btciapp&reviewId=gp%3AAOqpTOE83Fa_4e8yAHGLd4u4NXTsd_Tj8iJ-ZJDTJsibJFw987tNkwVz_mM3adWH9wvIvTN9--jTstHEgfo_NoY):

> This people are fraud u can only deposit money into it but cannot withdraw or transfer your coins. No customer supports no help desk. Beware of this fraud app!

At this point we assume the worst. At the very least it is a custodial offering
and thus **not verifiable** but ... don't install it. Just don't.