---
title: "Incognito - Anonymous Bitcoin Wallet"
altTitle: 

users: 10000
appId: com.incognito.wallet
launchDate: 
latestUpdate: 2020-10-05
apkVersionName: "3.10.0"
stars: 4.2
ratings: 169
reviews: 102
size: 52M
website: https://incognito.org
repository: https://github.com/incognitochain/incognito-wallet
issue: https://github.com/incognitochain/incognito-wallet/issues/1422
icon: com.incognito.wallet.png
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-07
reviewStale: false
signer: 
reviewArchive:


providerTwitter: incognitochain
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.incognito.wallet/
---


After many many reviews of custodial wallets, this one makes a promising claim:

> Don’t leave yourself exposed. Go Incognito. It’s non-custodial, decentralized,
  and completely open-source.

And indeed there is [a repository](https://github.com/incognitochain/incognito-wallet)
with 3647 commits by 16 contributors with recent commits. That looks solid!

Unfortunately the project appears to not share the needed `.env` file for
reproducible builds or otherwise build instructions for that purpose.

We reached out to them in
[this issue](https://github.com/incognitochain/incognito-wallet/issues/1422)
and hope to get feedback, soon but for now the app is **not verifiable**.
