---
title: "SWFT Blockchain"
altTitle: 

users: 10000
appId: com.swftcoin.client.android
launchDate: 
latestUpdate: 2020-12-02
apkVersionName: "5.1.1"
stars: 4.4
ratings: 1120
reviews: 687
size: 32M
website: http://www.swft.pro
repository: 
issue: 
icon: com.swftcoin.client.android.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-04-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: SwftCoin
providerLinkedIn: company/swftcoin
providerFacebook: SWFTBlockchain
providerReddit: 

redirect_from:
  - /com.swftcoin.client.android/
  - /posts/com.swftcoin.client.android/
---


The description on Google Play is full of buzzwords like big data, AI, machine
learning but no clear words on weather this wallet is custodial or not. Given
its strong emphasis on speed and many coins and no words on the usual seed words,
we have to assume it is indeed custodial. Their [FAQ](https://www.swft.pro/#/FAQ)
also sounds more like a custodial exchange than a wallet. This app is certainly
**not verifiable**.

