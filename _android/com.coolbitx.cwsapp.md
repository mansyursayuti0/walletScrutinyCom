---
title: "CoolBitX Crypto"
altTitle: 

users: 10000
appId: com.coolbitx.cwsapp
launchDate: 
latestUpdate: 2020-12-04
apkVersionName: "2.8.0"
stars: 4.6
ratings: 484
reviews: 231
size: 63M
website: http://coolwallet.io
repository: 
issue: 
icon: com.coolbitx.cwsapp.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-06
reviewStale: false
signer: 
reviewArchive:


providerTwitter: coolwallet
providerLinkedIn: 
providerFacebook: coolwallet
providerReddit: 

redirect_from:
  - /com.coolbitx.cwsapp/
---


> This app (made for the CoolWallet S -- the second generation card) is the most convenient and secure way to store, receive, and send your cryptocurrency assets.

This app is the companion app of a hardware wallet. If it doesn't manage private
keys itself, it's not a wallet. Let's see if it can be used without the
"CoolWallet S" hardware wallet ...

On Google Play there is no word about private keys.

At first sight, the website only talks about their hardware wallet.

As the app cannot be setup without Bluetooth being activated we assume it can't
be used as a wallet without the hardware wallet. It might still be able to have
hardware-less accounts which would make it a wallet again but as we can't test
that and most likely no significant funds would end up in such an account, we
file this app as a companion app of a hardware wallet, only.
