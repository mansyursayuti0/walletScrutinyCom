---
title: "Chameleon Pay"
altTitle: 

users: 1000
appId: com.chameleon.wallet
launchDate: 2019-02-22
latestUpdate: 2019-10-16
apkVersionName: "4.0.3"
stars: 4.5
ratings: 75
reviews: 61
size: 27M
website: https://www.chainzilla.io
repository: 
issue: 
icon: com.chameleon.wallet.png
bugbounty: 
verdict: defunct # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-12-08
reviewStale: false
signer: 
reviewArchive:


providerTwitter: chainzillaio
providerLinkedIn: 
providerFacebook: 
providerReddit: ChainZilla

redirect_from:
  - /com.chameleon.wallet/
  - /posts/com.chameleon.wallet/
---


**Update:** The app is not available on Google Play anymore.

This app claims to be "Non-Custodial" as their main key feature but is it open
source?

We find no such claim and while the company has a 
GitHub](https://github.com/ChainZilla), the app's source code
[cannot be found](https://github.com/search?q=org%3AChainZilla+%22com.chameleon.wallet%22&type=Code)
there. This wallet is therefore **not verifiable**.