---
title: "Cent - The crypto wallet for DeFi"
altTitle: 

users: 100
appId: com.atlas.mobile.wallet
launchDate: 
latestUpdate: 2020-10-29
apkVersionName: "1.0.11"
stars: 5.0
ratings: 15
reviews: 10
size: 13M
website: 
repository: 
issue: 
icon: com.atlas.mobile.wallet.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.atlas.mobile.wallet/
---


