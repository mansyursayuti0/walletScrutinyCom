---
title: "Binance: Bitcoin Marketplace & Crypto Wallet"
altTitle: 

users: 5000000
appId: com.binance.dev
launchDate: 2017-10-25
latestUpdate: 2020-12-03
apkVersionName: "1.32.2"
stars: 4.4
ratings: 63510
reviews: 25460
size: Varies with device
website: https://www.binance.com
repository: 
issue: 
icon: com.binance.dev.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-03-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: binance
providerLinkedIn: 
providerFacebook: binance
providerReddit: 

redirect_from:
  - /binance/
  - /com.binance.dev/
  - /posts/com.binance.dev/
---


This app appears to be an app interface for the Binance exchange and at first we
cannot find anything indicating that there is also a non-custodial feature to
this app.

> The Binance app is regularly updated to provide you with the most up to date
  features and to give you the best experience when trading on the binance
  crypto exchange platform. Whilst all funds are protected by SAFU (Secure Asset
  Fund for Users) so that you can be sure that all your digital assets are
  secure and safe in any situation, we are with you all the way to ensure you
  have the very best in customer service and security.

Doesn't sound like you control the keys.

(*Also Binance does provide a non-custodial wallet on Android which is
[Trustwallet](https://walletscrutiny.com/trust/) which happens to be closed
source.*)

This app being custodial is **not verifiable**.