---
title: "Hebe Wallet"
altTitle: 

users: 500
appId: com.hebeblock.hebewallet
launchDate: 
latestUpdate: 2020-11-25
apkVersionName: "1.2.21"
stars: 0.0
ratings: 
reviews: 
size: 22M
website: 
repository: 
issue: 
icon: com.hebeblock.hebewallet.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-04-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.hebeblock.hebewallet/
  - /posts/com.hebeblock.hebewallet/
---


This page was created by a script from the **appId** "com.hebeblock.hebewallet" and public
information found
[here](https://play.google.com/store/apps/details?id=com.hebeblock.hebewallet).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.