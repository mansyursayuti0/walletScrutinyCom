---
title: "Moonlet"
altTitle: 

users: 5000
appId: com.moonlet
launchDate: 
latestUpdate: 2020-12-07
apkVersionName: "1.4.22"
stars: 4.1
ratings: 117
reviews: 84
size: 9.0M
website: 
repository: 
issue: 
icon: com.moonlet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.moonlet/
---


This app appears to only support ETH tokens. Neither the description, nor the
website claim otherwise.
