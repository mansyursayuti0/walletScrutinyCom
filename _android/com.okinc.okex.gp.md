---
title: "OKEx - Bitcoin/Crypto Trading Platform"
altTitle: 

users: 100000
appId: com.okinc.okex.gp
launchDate: 
latestUpdate: 2020-12-02
apkVersionName: "4.4.8"
stars: 5.0
ratings: 28549
reviews: 21969
size: 109M
website: https://www.okex.com
repository: 
issue: 
icon: com.okinc.okex.gp.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: OKEx
providerLinkedIn: 
providerFacebook: okexofficial
providerReddit: OKEx

redirect_from:
  - /com.okinc.okex.gp/
  - /posts/com.okinc.okex.gp/
---


This app gives you access to a trading platform which sounds fully custodial and
therefore **not verifiable**.