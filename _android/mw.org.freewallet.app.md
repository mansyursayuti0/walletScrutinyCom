---
title: "Bitcoin & Crypto Blockchain Wallet: Freewallet"
altTitle: 

users: 100000
appId: mw.org.freewallet.app
launchDate: 2017-08-10
latestUpdate: 2020-12-03
apkVersionName: "1.14.15"
stars: 4.1
ratings: 6883
reviews: 4420
size: 12M
website: https://freewallet.org
repository: 
issue: 
icon: mw.org.freewallet.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: freewalletorg
providerLinkedIn: 
providerFacebook: freewallet.org
providerReddit: 

redirect_from:
  - /mw.org.freewallet.app/
  - /posts/mw.org.freewallet.app/
---


According to the description

> In addition, the majority of cryptocurrency assets on the platform are stored in an offline vault. Your coins will be kept in cold storage with state of the art security protecting them.

this is a custodial app.

Our verdict: **not verifiable**.
