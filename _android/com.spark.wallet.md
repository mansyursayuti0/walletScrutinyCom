---
title: "Spark Lightning Wallet"
altTitle: 

users: 500
appId: com.spark.wallet
launchDate: 2018-12-10
latestUpdate: 2020-09-03
apkVersionName: "0.0.0"
stars: 4.6
ratings: 8
reviews: 3
size: 8.0M
website: https://github.com/shesek/spark-wallet
repository: 
issue: 
icon: com.spark.wallet.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.spark.wallet/
  - /posts/com.spark.wallet/
---


