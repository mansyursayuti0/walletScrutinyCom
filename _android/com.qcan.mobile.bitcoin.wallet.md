---
title: "Mobile Bitcoin Wallet"
altTitle: 

users: 10000
appId: com.qcan.mobile.bitcoin.wallet
launchDate: 
latestUpdate: 2020-11-11
apkVersionName: "0.8.848"
stars: 4.3
ratings: 104
reviews: 71
size: 28M
website: https://qcan.com
repository: 
issue: 
icon: com.qcan.mobile.bitcoin.wallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-08
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.qcan.mobile.bitcoin.wallet/
---


> **Complete Control**<br>
  Your Bitcoin, 100% Under Your Control. You hold the key. No intermediary.

That is a clear claim to be non-custodial but neither on Google Play nor the
website can we find a link to source code. This app is thus **not verifiable**.
