---
title: "KyberSwap: Buy, Trade, Transfer Cryptocurrencies"
altTitle: 

users: 10000
appId: com.kyberswap.android
launchDate: 
latestUpdate: 2020-10-16
apkVersionName: "1.1.33"
stars: 4.2
ratings: 1714
reviews: 824
size: 19M
website: https://kyberswap.com
repository: 
issue: 
icon: com.kyberswap.android.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: KyberSwap
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.kyberswap.android/
---


> KyberSwap as an Ethereum DEX (decentralised exchange), does not hold custody of your assets or digital currencies, so you are always in full control.

is an implied claim of being non-custodial but "KyberSwap as an Ethereum DEX"
refers to an ETH smart contract and not to this Android wallet of the same name.

> Exchange ETH and over 70+ tokens including DAI, USDC, WBTC, MKR, BAT, LINK.

as it turns out, this is not a BTC wallet. Only ETH and ETH tokens.