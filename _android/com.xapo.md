---
title: "Xapo"
altTitle: 

users: 1000000
appId: com.xapo
launchDate: 2014-04-01
latestUpdate: 2020-07-29
apkVersionName: "5.30"
stars: 3.8
ratings: 45116
reviews: 21288
size: 93M
website: https://xapo.com
repository: 
issue: 
icon: com.xapo.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-10-12
reviewStale: false
signer: 
reviewArchive:


providerTwitter: xapo
providerLinkedIn: 
providerFacebook: xapoapp
providerReddit: 

redirect_from:
  - /xapo/
  - /com.xapo/
  - /posts/2019/11/xapo/
  - /posts/com.xapo/
---


Xapo describes itself
as a wallet:

> We’re a digital wallet that allows you to easily and safely send, receive,
> store and spend any traditional currencies and bitcoin.

but under closer investigation, we can't find any Bitcoin wallet here. Naming
"traditional currencies" and "bitcoin" in the same security
claim sound like it has to be custodial.

Their [website on security](https://xapo.com/en/security) is not claiming
otherwise neither which makes us conclude: This app is **not verifiable**.
