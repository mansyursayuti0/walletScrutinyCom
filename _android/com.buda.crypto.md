---
title: "Buda.com - Bitcoin wallet. Compra, vende, almacena"
altTitle: 

users: 10000
appId: com.buda.crypto
launchDate: 
latestUpdate: 2020-06-02
apkVersionName: "1.9.10"
stars: 3.0
ratings: 254
reviews: 168
size: 12M
website: https://www.buda.com
repository: 
issue: 
icon: com.buda.crypto.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BudaPuntoCom
providerLinkedIn: company/9294718
providerFacebook: BudaPuntoCom
providerReddit: 

redirect_from:
  - /com.buda.crypto/
  - /posts/com.buda.crypto/
---


This app is an interface to an exchange and coins are held there and not on the
phone. As a custodial service it is **not verifiable**.