---
title: "Bitcoin Wallet BitBucks"
altTitle: 

users: 5000
appId: de.fuf.bitbucks
launchDate: 2019-09-03
latestUpdate: 2020-05-20
apkVersionName: "1.1.7"
stars: 4.3
ratings: 30
reviews: 11
size: 8.9M
website: https://www.bitbucks.io
repository: 
issue: 
icon: de.fuf.bitbucks.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-30
reviewStale: true
signer: 
reviewArchive:


providerTwitter: bit_bucks
providerLinkedIn: 
providerFacebook: bitbucks.io
providerReddit: 

redirect_from:
  - /de.fuf.bitbucks/
  - /posts/de.fuf.bitbucks/
---


This app appears to be a custodial service as per their description:

> The BitBucks Wallet enables instant payments with bitcoin. Users register at
  BitBucks just by entering their phone numbers followed by a validation code
  sended to them via sms.
> 
> Afterwards users can top-up their BitBucks account with bitcoin by using any
  other Bitcoin wallet. Every transaction within the BitBucks app, as well as
  the top-up of the account, are free of charge.

And from their website:

> **Pay safely and securely**
> 
> Your Bitcoin is multi-signature protected and will be stored in the safest
  wallets. Even if you lose your mobile phone, you will not lose your credit.

This is a custodial app.

Our verdict: **not verifiable**.