---
title: "Bitcoin Wallet Hexa"
altTitle: 

users: 500
appId: io.hexawallet.hexa
launchDate: 
latestUpdate: 2020-12-02
apkVersionName: "1.3.2"
stars: 3.5
ratings: 15
reviews: 10
size: 39M
website: 
repository: 
issue: 
icon: io.hexawallet.hexa.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-08-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.hexawallet.hexa/
---


This page was created by a script from the **appId** "io.hexawallet.hexa" and public
information found
[here](https://play.google.com/store/apps/details?id=io.hexawallet.hexa).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.