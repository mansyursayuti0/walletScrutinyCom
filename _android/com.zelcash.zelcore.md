---
title: "ZelCore - Multi Asset Crypto Wallet"
altTitle: 

users: 10000
appId: com.zelcash.zelcore
launchDate: 2018-09-21
latestUpdate: 2020-12-09
apkVersionName: "4.1.0"
stars: 3.7
ratings: 320
reviews: 221
size: 15M
website: https://zel.network/project/zelcore
repository: 
issue: 
icon: com.zelcash.zelcore.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-04-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: zelcash
providerLinkedIn: 
providerFacebook: 
providerReddit: ZelCash

redirect_from:
  - /com.zelcash.zelcore/
  - /posts/com.zelcash.zelcore/
---


This application claims on their Google Play description:

> Ultimate security provided with no personal user information being stored
> off-device.

and

> Single master seed backup (This will be the only seed you will ever need in
> the future for any coin we integrate)

which sounds like a non-custodial wallet but neither on Google Play nor on their
website do we find a link to the source code, so their claims are **not
verifiable**.
