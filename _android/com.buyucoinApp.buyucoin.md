---
title: "BuyUcoin - Popular Indian Crypto Currency Exchange"
altTitle: 

users: 5000
appId: com.buyucoinApp.buyucoin
launchDate: 
latestUpdate: 2020-12-12
apkVersionName: "3.2"
stars: 4.8
ratings: 205
reviews: 169
size: 17M
website: https://www.buyucoin.com
repository: 
issue: 
icon: com.buyucoinApp.buyucoin.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-06-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: buyucoin
providerLinkedIn: company/buyucoin
providerFacebook: BuyUcoin
providerReddit: 

redirect_from:
  - /com.buyucoinApp.buyucoin/
  - /posts/com.buyucoinApp.buyucoin/
---


This app appears to be the broken interface for a broken exchange, judging by
the vast majority of user comments. It is certainly **not verifiable**.
