---
title: "Flare Wallet"
altTitle: 

users: 1000
appId: com.flare
launchDate: 2020-02-22
latestUpdate: 2020-11-29
apkVersionName: "1.3.7"
stars: 4.4
ratings: 213
reviews: 176
size: 9.8M
website: https://flarewallet.io
repository: 
issue: 
icon: com.flare.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-08-04
reviewStale: true
signer: 
reviewArchive:


providerTwitter: flarewallet
providerLinkedIn: 
providerFacebook: FlareWallet
providerReddit: 

redirect_from:
  - /com.flare/
  - /posts/com.flare/
---


This app claims to be non-custodial:

> Non-Custodial
> 
> Retain complete control over all of your private keys.

but we cannot find any mention of source code on their website or Google Play
description so the app is **not verifiable**.
