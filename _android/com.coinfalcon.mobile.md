---
title: "CoinFalcon – Buy & sell Bitcoin"
altTitle: 

users: 10000
appId: com.coinfalcon.mobile
launchDate: 
latestUpdate: 2020-12-11
apkVersionName: "2.1.11"
stars: 3.4
ratings: 77
reviews: 52
size: 64M
website: https://coinfalcon.com
repository: 
issue: 
icon: com.coinfalcon.mobile.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: coinfalcon
providerLinkedIn: 
providerFacebook: CoinFalcon
providerReddit: CoinFalcon

redirect_from:
  - /com.coinfalcon.mobile/
---


On Google Play we read:

> **SECURE YOUR WEALTH**<br>
  Don’t give control of your private keys to centralized wallets and exchanges
  that can suffer from hacks and lose your funds. **Exodus** encrypts your
  private keys and transaction data on your device so that no one can access
  your cryptocurrency but you. You can also Enable Face or Touch ID to
  conveniently secure your wallet without having to type your passcode.

This is confusing. Are they inviting the user to use Exodus or did they copy
Exodus promo texts? Cause below that we read:

> **State-of-the-Art Security**<br>
  CoinFalcon stores 98% of digital funds in an offline, secure wallet, while the
  rest is protected by high-grade security systems. We are committed to the
  highest safety standards both here on the app and our web platform.

which is clearly about a custodial offering.