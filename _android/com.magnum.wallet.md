---
title: "Magnum Wallet – Bitcoin, Ethereum, Crypto Exchange"
altTitle: 

users: 10000
appId: com.magnum.wallet
launchDate: 2019-04-23
latestUpdate: 2019-08-29
apkVersionName: "1.0.12"
stars: 3.7
ratings: 200
reviews: 141
size: 3.0M
website: https://magnumwallet.co
repository: 
issue: 
icon: com.magnum.wallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-04-07
reviewStale: false
signer: 
reviewArchive:


providerTwitter: magnum_wallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.magnum.wallet/
  - /posts/com.magnum.wallet/
---


In the app's description we read:

> **Simple & Secure Interface**
> Stay on top of your portfolio with intuitive navigation anytime, anywhere.
> With security & anonymity as our top priorities, we provide a fully
> non-custodial service, meaning that users have full control of their private
> keys and other personal information.

So this wallet claims to be non-custodial but can we find its source code?
Neither on Google Play nor on their website do we find a word about this product's
source code. Its properties thus are **not verifiable**.