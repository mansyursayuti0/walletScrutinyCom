---
title: "Coinpay: Buy & Send Bitcoin Fast Cryptocurrency 😇"
altTitle: 

users: 1000
appId: com.coinpay
launchDate: 
latestUpdate: 2020-08-23
apkVersionName: "1.0.7"
stars: 4.3
ratings: 11
reviews: 9
size: 5.4M
website: https://www.coinpayapp.com
repository: 
issue: 
icon: com.coinpay.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.coinpay/
---


> We are a non-custodial wallet, so users can send Bitcoin and other
  cryptocurrencies globally with direct access to the blockchain.

... so this is (claiming to be) a non-custodial Bitcoin wallet. Can we verify
this?

The answer is "no". There is no source code linked on their website or Google
Play description. This app is closed source and thus **not verifiable**.