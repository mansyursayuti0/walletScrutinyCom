---
title: "Vega - Lightning Wallet"
altTitle: 

users: 50
appId: app.getvega
launchDate: 
latestUpdate: 2019-05-22
apkVersionName: "Varies with device"
stars: 
ratings: 
reviews: 
size: Varies with device
website: https://getvega.app
repository: 
issue: 
icon: app.getvega.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-29
reviewStale: false
signer: 
reviewArchive:


providerTwitter: GetVegaApp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /app.getvega/
  - /posts/app.getvega/
---


