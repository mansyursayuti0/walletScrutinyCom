---
title: "Utrust Wallet"
altTitle: 

users: 5000
appId: com.uwalletapp
launchDate: 
latestUpdate: 2020-06-08
apkVersionName: "1.4.2"
stars: 4.3
ratings: 104
reviews: 57
size: 80M
website: https://utrust.com
repository: 
issue: 
icon: com.uwalletapp.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: false
signer: 
reviewArchive:


providerTwitter: utrust
providerLinkedIn: company/utrust-payments
providerFacebook: utrust.io
providerReddit: UTRUST_Official

redirect_from:
  - /com.uwalletapp/
---


> The Utrust Wallet processes transactions instantly within our platform, which
  allows us to cut on blockchain validation waiting times.

and

> Instant transactions to Utrust Wallet users

are clear features of a custodial app. This wallet is **not verifiable**.